from machine import Pin, PWM
from utime import sleep
#from dcmotor import DCMOTOR
#enable = PWM(Pin(12), 15000)
#SERVO MOTOR VARIABLES
X_Axis = PWM(Pin(19), freq=50)
Y_Axis = PWM(Pin(22), freq=50)
rotation = PWM(Pin(23), freq=50)
fingers = PWM(Pin(21), freq=50)



D1_PHA_A = Pin(17, Pin.OUT)
D1_EN_A = Pin(16, Pin.OUT)

D1_STBY = Pin(4, Pin.OUT)

D1_PHA_B = Pin(0, Pin.OUT)
D1_EN_B = Pin(15, Pin.OUT)

D2_EN_A = Pin(12, Pin.OUT)
D2_PHA_A = Pin(13, Pin.OUT)

D2_STBY = Pin(14, Pin.OUT)

D2_EN_B = Pin(26, Pin.OUT)
D2_PHA_B = Pin(27, Pin.OUT)

while True:
    fingers.duty(70)
    sleep(2)
    fingers.duty(80)
    sleep(3)
    """D2_STBY.on()
    D2_EN_B.on()
    D2_PHA_B.off()
    
    D2_EN_A.on()
    D2_PHA_A.off()
    sleep(2)
    
    D2_STBY.off()
    D2_EN_B.off()
    D2_PHA_B.off()
    
    D2_EN_A.off()
    D2_PHA_A.off()
    sleep(5)
    D2_STBY.on()
    D2_EN_B.on()
    D2_PHA_B.on()
    D2_EN_A.on()
    D2_PHA_A.on()
    
    sleep(2)
    D2_EN_B.off()
    D2_PHA_B.off()
    D2_STBY.off()
    D2_EN_A.off()
    D2_PHA_A.off()
    sleep(5)
""""""

while True:
    D1_STBY.on()
    
    D1_EN_B.on()
    D1_PHA_B.on()
    
    D1_EN_A.off()
    D1_PHA_A.off()
    
    sleep(2)
    
    D1_EN_B.off()
    D1_PHA_B.off()
    D1_STBY.off()
    D1_EN_A.off()
    D1_PHA_A.off()
    
    sleep(5)
    
    D1_EN_B.on()
    D1_PHA_B.on()
    D1_EN_A.on()
    D1_PHA_A.on()
    D1_STBY.on()
    sleep(2)
    D1_EN_B.off()
    D1_PHA_B.off()
    D1_STBY.off()
    D1_EN_B.off()
    D1_PHA_B.off()
    sleep(5)"""