from pyfingerprint import PyFingerprint
from machine import UART
import globalVars as globalvars
from pyfingerprint import FINGERPRINT_CHARBUFFER1
from pyfingerprint import FINGERPRINT_CHARBUFFER2
from sys import exit
sensorSerial = UART(1)
# ESP32 (pins 12, 13)
sensorSerial.init(57600, bits=8, parity=None, stop=1, rx=13, tx=12)
# pyboard v1.1 (pins X9, X10)
# sensorSerial.init(57600, bits=8, parity=None, stop=1)

f = PyFingerprint(sensorSerial)

while True:
    try:
        print("Number of fingerprints saved:",f.getTemplateCount(),"/",f.getStorageCapacity())
        print("Waiting for finger...")
        while f.readImage() == False:
            pass

        print("Read 1 OK")

        val = f.convertImage(FINGERPRINT_CHARBUFFER1)
        print("Convert 1:", val)

        result = f.searchTemplate()
        positionNumber = result[0]

        if ( positionNumber >= 0 ):
            print('Template already exists at position #' + str(positionNumber))
            exit()
            
        print("Press same finger unto scanner")
        while f.readImage() == False:
            pass

        print("Read 2 OK")
        val = f.convertImage(FINGERPRINT_CHARBUFFER2)
        print("Convert 2:", val)

        if ( f.compareCharacteristics() == 0 ):
            raise Exception('Fingers do not match')
        
        print("Creating template")
        f.createTemplate()

        print("Storing template")
        store = f.storeTemplate()
        print("Stored at pos:", store)
    
    except KeyboardInterrupt:
        exit()
