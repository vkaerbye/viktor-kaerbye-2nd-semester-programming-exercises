from pyfingerprint import PyFingerprint
from machine import UART
from pyfingerprint import FINGERPRINT_CHARBUFFER1
from pyfingerprint import FINGERPRINT_CHARBUFFER2
from sys import exit
import hashlib
sensorSerial = UART(1)
# ESP32 (pins 12, 13)
sensorSerial.init(57600, bits=8, parity=None, stop=1, rx=13, tx=12)
f = PyFingerprint(sensorSerial)
print("Number of fingerprints saved:",f.getTemplateCount(),"/",f.getStorageCapacity())


while True:
    try:
        print("Waiting for finger...\n")
        while f.readImage() == False:
            pass
        print("Finger detected")
        f.convertImage(FINGERPRINT_CHARBUFFER1)
        (pos, acc) = f.searchTemplate()
        if pos < 0:
            print("No match was found")
        else:
            print("The fingerprint is saved in position", pos, "and has an accuracy score of", acc)
            """ GETTING HASH - DOESN'T WORK
            f.loadTemplate(pos, FINGERPRINT_CHARBUFFER1)
            characteristics = str(f.downloadCharacteristics(FINGERPRINT_CHARBUFFER1)).encode('utf-8')
            print("Hash of fingerprint: ", hashlib.sha256(characteristics).hexdigest())"""
    except KeyboardInterrupt:
        exit()
    except Exception:
        print("Please try again")
        pass
