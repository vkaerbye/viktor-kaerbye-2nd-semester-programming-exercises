from pyfingerprint import PyFingerprint
from machine import UART
import globalVars as globalvars
from pyfingerprint import FINGERPRINT_CHARBUFFER1
from pyfingerprint import FINGERPRINT_CHARBUFFER2
from sys import exit
sensorSerial = UART(1)
# ESP32 (pins 12, 13)
sensorSerial.init(57600, bits=8, parity=None, stop=1, rx=13, tx=12)
# pyboard v1.1 (pins X9, X10)
# sensorSerial.init(57600, bits=8, parity=None, stop=1)

f = PyFingerprint(sensorSerial)
print("Number of fingerprints saved:",f.getTemplateCount(),"/",f.getStorageCapacity())
while True:
    try:
        print("FINGERPRINT DELETION MODE")
        print("Waiting for finger...")
        while f.readImage() == False:
            pass
        print("Fingerprint read!")
        f.convertImage(FINGERPRINT_CHARBUFFER1)
        (pos, acc) = f.searchTemplate()
        if pos < 0:
            print("No match was found")
        elif f.deleteTemplate(pos) == True:
            print("The fingerprint saved in position", pos, "has been deleted")
        else:
            print("An error occurred when trying to delete fingerprint no.", pos)
    except KeyboardInterrupt:
        exit()