from hcsr04 import HCSR04
from time import sleep
from machine import PWM, Pin

motorR = PWM(Pin(X))
motorL = PWM(Pin(Y))

motorR.freq(500)
motorL.freq(500)

sensorR = HCSR04(trigger_pin = 27, echo_pin = 14, echo_timeout_us = 10000)
sensorL = HCSR04(trigger_pin = X, echo_pin = Y, echo_timeout_us = 10000)

def feedback(distanceL, distanceR):
    if distanceL > distanceR: #I want to change this to be 20% more
        motorR.duty(800)
        motorL.duty(1000)
    elif distanceR > distanceL: #I want to change this to be 20% more
        motorR.duty(1000)
        motorL.duty(800)
    else:
        motorR.duty(1000)
        motorL.duty(1000)
while True:
    distanceL = sensorL.distance_cm()
    distanceR = sensorR.distance_cm()
    feedback(distanceL, distanceR)
    print('Position:\n', distanceL, 'cm from L\n', distanceR, "cm from R" )
    sleep(1)