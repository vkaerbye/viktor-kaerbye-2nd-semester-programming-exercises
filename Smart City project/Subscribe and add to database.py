import sqlite3
from time import sleep
from socket import error as socket_error
import paho.mqtt.client as mqtt
import json
from datetime import datetime


# VARIABLES
clientId = "VkaerbyeMacBook"
broker_addr = 'localhost'  
broker_port = 1883 # for tcp if port 1883 is open
subscribe_topic = "sensordata"
client = mqtt.Client(client_id=clientId, clean_session=1, transport='tcp') # init client object
conn = sqlite3.connect(r'values.db', check_same_thread=False) #Creating the 'conn' object from the sqlite3 class. The 'r' before the filename ensures,
#that if the file doesn't exist, it will be created. Basically it creates the connection to the database. 
c = conn.cursor() #Instantiating the 'c' object which is basically just a pointer to the dabase object. 
sqlCreateTable = """CREATE TABLE MEASUREMENTS(
    MESASUREMENT_NUM INTEGER PRIMARY KEY AUTOINCREMENT,
    TIMESTAMP STRING,
    TEMPERATURE FLOAT NOT NULL,
    HUMIDITY FLOAT NOT NULL,
    LIGHT FLOAT NOT NULL,
    GAS FLOAT NOT NULL,
    DUST FLOAT NOT NULL,
    SOUND FLOAT NOT NULL
)"""
delete = """DROP TABLE IF EXISTS MEASUREMENTS"""




# triggers when message received from broker
def on_message(client, userdata, message):
    message_decoded = json.loads(message.payload.decode('utf-8'))
    mes_dict = dict(message_decoded)
    light = mes_dict['light']
    temp = mes_dict['temperature']
    hum = mes_dict['humidity']
    gas = mes_dict['gas']
    sound = mes_dict['sound']
    dust = mes_dict['dust']
    timestamp = str(datetime.now())
    #conn = sqlite3.connect('values.db')
    #c = conn.cursor()
    print("Message recieved:", light, temp, hum, gas, sound, dust)
    #print(f'- message received, {message_decoded}\n')
    c.execute("""INSERT INTO MEASUREMENTS(TIMESTAMP, TEMPERATURE, HUMIDITY, LIGHT, GAS, DUST, SOUND) VALUES(?,?,?,?,?,?,?)""", (timestamp, temp, hum, light, gas, dust, sound))
    conn.commit()


# triggers when connected to mqtt broker
def on_connect(client, userdata, flags, rc):
    print(f'- client: {client} connected to {broker_addr} on port {broker_port}, userdata: {userdata}, flags: {flags}, rc: {rc}\n')

# attach callbacks
client.on_connect = on_connect # attach callback function
client.on_message = on_message # attach message callback to callback

# MAIN PROGRAM

try: 
    print('- connecting to broker')
    client.connect(host=broker_addr, port=broker_port)
    client.subscribe(subscribe_topic, qos=0)
    client.loop_start() #start the event loop
    c.execute(delete)
    print("Dropped table")
    c.execute(sqlCreateTable)
    print("Created new table successfully...")

except socket_error as e:
    print(f'- could not connect {clientId} to {broker_addr} on port {broker_port}\n {e}')
    exit(0)

# publish
while(True):
    try:
        # nothing here as subsrcibed message is handled in the on_message callback
        pass

    except KeyboardInterrupt: #cleanup nice
        client.loop_stop() #stop the loop
        exit(0)

