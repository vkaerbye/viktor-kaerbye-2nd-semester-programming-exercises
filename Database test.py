import sqlite3
import random
from time import sleep
import dht11
import RPi.GPIO as GPIO


sensor = dht11.DHT11(pin = 17) #Creating the sensor object from the dht11 class

conn = sqlite3.connect(r'values.db') #Creating the 'conn' object from the sqlite3 class. The 'r' before the filename ensures,
#that if the file doesn't exist, it will be created. Basically it creates the connection to the database. 
c = conn.cursor() #Instantiating the 'c' object which is basically just a pointer to the dabase object. 

sql = """CREATE TABLE MEASUREMENTS(
    MESASUREMENT_NUM INTEGER PRIMARY KEY AUTOINCREMENT,
    TEMPERATURE INT NOT NULL,
    HUMIDITY INT NOT NULL,
)"""
#This is the first SQL statement that's creating the table 'measurements', and adding three columns to it; 'MEASUREMENT_NUM', 'TEMPERATURE' and 'HUMIDITY'

delete = """DROP TABLE IF EXISTS MEASUREMENTS""" #SQL Statement that deletes the table if it exists (Debugging)


#Creating a function t pull data from the sensor
def createData():
    data = sensor.read()
    temp = data.temperature
    hum = data.humidity
    return temp, hum


#c.execute executes the sql statement in question, but it's not final until conn.commit() has been run.
#Below, a lot of stuff has been tried out. That's why some of it is commented out. 
c.execute(delete)
#print("Dropped table")
c.execute(sql)
#print("Created table successfully...")

#The below for loop inserts 1000 readings from the sensor into the table in the .db file.
for i in range(1000):
    try:
        c.execute("""INSERT INTO MEASUREMENTS(TEMPERATURE, HUMIDITY) VALUES(?,?)""", createData())
        #sleep(2)
    except KeyboardInterrupt:
        exit()
print("Inserted values successfully...")

#Commit executes the changes, so that they're actually added to the table. 
conn.commit()