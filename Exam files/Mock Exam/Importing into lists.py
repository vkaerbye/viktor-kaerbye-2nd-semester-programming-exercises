import matplotlib.pyplot as plt
import numpy as np

fhand1k = open("1k.txt", "r")
fhand10k = open("10k.txt", "r")
fhand100k = open("100k.txt","r")

readings1k = []
readings10k = []
readings100k = []


for line in fhand1k:
    readings1k.append(int(line.strip()))

for line in fhand10k:
    readings10k.append(int(line.strip()))

for line in fhand100k:
    readings100k.append(int(line.strip()))

print(readings1k)
print(readings10k)
print(readings100k)

x = np.linspace(0,25,25)
plt.plot(x, readings1k)
plt.title("1kOhm resistor values")
plt.show()

Vin = 3.3
Rs = 1000
Rt = 5000
avg1k = np.average(readings1k)
Vout = (Rs/(Rs+Rt))*Vin
print("Average value measured:", avg1k)
print("1kOhm resistor:",Vout)

Vin = 3.3
Rs = 10000
Rt = 5000
avg10k = np.average(readings10k)
Vout = (Rs/(Rs+Rt))*Vin
print("Average value measured:", avg10k)
print("10kOhm resistor:",Vout)

Vin = 3.3
Rs = 100000
Rt = 5000
avg100k = np.average(readings100k)
Vout = (Rs/(Rs+Rt))*Vin
print("Average value measured:", avg100k)
print("100kOhm resistor:",Vout)