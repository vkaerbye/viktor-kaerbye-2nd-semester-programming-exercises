import numpy as np

Vin = 3.3
Rs = 1000
Rt = 5000

Vout = (Rs/(Rs+Rt))*Vin

print(Vout)

Vin = 3.3
Rs = 10000
Rt = 5000

Vout = (Rs/(Rs+Rt))*Vin

print(Vout)

Vin = 3.3
Rs = 100000
Rt = 5000

Vout = (Rs/(Rs+Rt))*Vin

print(Vout)