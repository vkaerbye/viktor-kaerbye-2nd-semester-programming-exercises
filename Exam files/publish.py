'''
Inspiration from http://www.steves-internet-guide.com/into-mqtt-python-client/
created by: Nikolaj Simonsen | November 2021
repo: https://gitlab.com/npes-py-experiments/mqtt-thingspeak

paho-mqtt lib reference: https://pypi.org/project/paho-mqtt/
'''

# IMPORTS

from functions import get_tokens
from socket import error as socket_error
import paho.mqtt.client as mqtt
import time
from datetime import datetime
import random

# VARIABLES


broker_addr = '192.168.137.167'  
broker_port = 1883 # for tcp if port 1883 is open
#broker_port = 80 # for websocket if port 1883 is blocked
publish_topic = 'test'
publish_interval = 15
clientId = "vkaerbyePublisher"
client = mqtt.Client(client_id=clientId, clean_session=1, transport='tcp') # init client object
#client = mqtt.Client(client_id=clientId, clean_session=1, transport='websockets') # init client object using websockets if port 1883 is blocked

# FUNCTIONS

# triggers when connected to mqtt broker
def on_connect(client, userdata, flags, rc):
    print(f'- client: {client} connected to {broker_addr} on port {broker_port}, userdata: {userdata}, flags: {flags}, rc: {rc}\n')
    # print(f'client: {client}, userdata: {userdata}, flags: {flags}, rc: {rc}')

# create payload
def build_payload(sensor_reading_1=0, sensor_reading_2=0, sensor_id=None):
    timestamp = datetime.utcnow().timestamp() # utc timestamp
    payload = f'field1={sensor_reading_1}&field2={sensor_reading_2}'
    return payload

# read the sensor
def read_sensor():
    sensor_reading = random.randrange(0,10) # fake sensor date, replace with real sensor reading
    return sensor_reading

# attach callbacks
client.on_connect = on_connect # attach callback function

# MAIN PROGRAM

try: 
    print('- connecting to broker')
    client.connect(host=broker_addr, port=broker_port)
    client.loop_start() #start the event loop

except socket_error as e:
    print(f'- could not connect {clientId} to {broker_addr} on port {broker_port}\n {e}')
    exit(0)

# publish
while(True):
    try:    
        payload = build_payload(sensor_reading_1=read_sensor(), sensor_reading_2=read_sensor())
        if client.is_connected():
            print(f'- Publishing {payload} to topic, {publish_topic}\n')
            client.publish(publish_topic, payload)        
            time.sleep(publish_interval) # wait x seconds

    except KeyboardInterrupt: #cleanup nice
        client.loop_stop() #stop the loop
        exit(0)
