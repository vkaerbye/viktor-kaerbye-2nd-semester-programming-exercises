#ldr setup
#log data to file

import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BOARD)
pin_to_circuit = 7
readings =[]

#log to text file
def log_data():
        try:
            file = open("light_sensor.txt", "w") #substitute with 10k.txt and 100k.txt
            for i in readings:
                file.write(str(i) + '\n')
        except KeyboardInterrupt:
            file.close()
            exit()

def rc_time (pin_to_circuit):
    count = 0
  
    #Output on the pin for 
    GPIO.setup(pin_to_circuit, GPIO.OUT)
    GPIO.output(pin_to_circuit, GPIO.LOW)
    time.sleep(0.5)

    #Change the pin back to input
    GPIO.setup(pin_to_circuit, GPIO.IN)
  
    #Count until the pin goes high
    while (GPIO.input(pin_to_circuit) == GPIO.LOW):
        count += 1

    return count

#Catch when script is interrupted, cleanup correctly
try:
    # Main loop
    while True:
        print(rc_time(pin_to_circuit))
        readings.append(rc_time(pin_to_circuit))
        if len(readings) == 25:
            log_data()
            break
except KeyboardInterrupt:
    pass
finally:
    GPIO.cleanup()
