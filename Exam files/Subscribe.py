# IMPORTS

from socket import error as socket_error
import paho.mqtt.client as mqtt
import json

# VARIABLES
clientId = "VkaerbyeMacBook"
broker_addr = '192.168.137.167'  
broker_port = 1883 # for tcp if port 1883 is open
subscribe_topic = "test"
client = mqtt.Client(client_id=clientId, clean_session=1, transport='tcp') # init client object

# FUNCTIONS

# triggers when message received from broker
def on_message(client_id, userdata, message):
    message_decoded = message.payload.decode('utf-8') #For use with regular string payloads
    #message_decoded = json.loads(message.payload.decode('utf-8')) #For use with JSON payloads
    print(f'- message received, {message_decoded}\n')

# triggers when connected to mqtt broker
def on_connect(client, userdata, flags, rc):
    print(f'- client: {client} connected to {broker_addr} on port {broker_port}, userdata: {userdata}, flags: {flags}, rc: {rc}\n')

# attach callbacks
client.on_connect = on_connect # attach callback function
client.on_message = on_message # attach message callback to callback

# MAIN PROGRAM

try: 
    print('- connecting to broker')
    client.connect(host=broker_addr, port=broker_port)
    client.subscribe(subscribe_topic, qos=0)
    client.loop_start() #start the event loop

except socket_error as e:
    print(f'- could not connect {clientId} to {broker_addr} on port {broker_port}\n {e}')
    exit(0)

# publish
while(True):
    try:
        # nothing here as subsrcibed message is handled in the on_message callback
        pass

    except KeyboardInterrupt: #cleanup nice
        client.loop_stop() #stop the loop
        exit(0)

