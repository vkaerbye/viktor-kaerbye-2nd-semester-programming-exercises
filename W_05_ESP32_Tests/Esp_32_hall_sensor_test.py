from time import sleep
from machine import Pin, PWM
import esp32

freq = 5000
led = PWM(Pin(23), freq)

while True:
    print(esp32.hall_sensor())
    led.duty(esp32.hall_sensor())
    sleep(1)
    
    