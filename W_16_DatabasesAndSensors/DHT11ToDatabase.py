import sqlite3
import RPi.GPIO as GPIO #Import the Raspberry Pi GPIO library to utilize the GPIO Pins
import time
GPIO.setmode(GPIO.BCM) # Setting the way GPIO pins are defined. 
delay = 0.5 # Value determining how long the program should wait when measuring the light level
value = 0 # Initial light value
ldr = 8 # The pin number of the GPIO Pin reading the light level from the LDR
led = 10 # The pin number of the GPIO Pin controlling the LED
GPIO.setup(led, GPIO.OUT) # Setting the LED Control GPIO Pin to be an output pin
GPIO.output(led, False) # Initially turning off the LED
def rc_time (ldr): # Function for reading the light level utilizing the capacitor.
    count = 0 # Initial count value
    GPIO.setup(ldr, GPIO.OUT) # Setting up the LDR Pin as an output pin
    GPIO.output(ldr, False) # Turning off the LDR pin
    time.sleep(delay) # Wait delay time
    GPIO.setup(ldr, GPIO.IN) # Changing the LDR Pin to function as an input pin
    while (GPIO.input(ldr) == 0): # While the voltage from the LDR (Capacitor) is below the threshold...
        count += 1 # Add one to the variable "count"
    return count # In the end, return the counted value. 


conn = sqlite3.connect(r'values.db') #Creating the 'conn' object from the sqlite3 class. The 'r' before the filename ensures,
#that if the file doesn't exist, it will be created. Basically it creates the connection to the database. 
c = conn.cursor() #Instantiating the 'c' object which is basically just a pointer to the dabase object. 

sql = """CREATE TABLE capmeasurements(
    cap1U INT NOT NULL,
    cap1C INT NOT NULL,
    cap2U INT NOT NULL,
    cap2C INT NOT NULL,
    cap4U INT NOT NULL,
    cap4C INT NOT NULL,
)"""
#This is the first SQL statement that's creating the table 'capmeasurements', and adding six columns to it.

delete = """DROP TABLE IF EXISTS MEASUREMENTS""" #SQL Statement that deletes the table if it exists (Debugging)


cap1U = []
cap1C = []
cap2U = []
cap2C = []
cap4U = []
cap4C = []
for i in range(20):
    cap1U.append(rc_time(ldr))


#c.execute executes the sql statement in question, but it's not final until conn.commit() has been run.
#Below, a lot of stuff has been tried out. That's why some of it is commented out. 
c.execute(delete)
#print("Dropped table")
c.execute(sql)
#print("Created table successfully...")

#The below for loop inserts 1000 readings from the sensor into the table in the .db file.
for i in range(1000):
    try:
        c.execute("""INSERT INTO MEASUREMENTS(TEMPERATURE, HUMIDITY) VALUES(?,?)""", createData())
        #sleep(2)
    except KeyboardInterrupt:
        exit()
print("Inserted values successfully...")

#Commit executes the changes, so that they're actually added to the table. 
conn.commit()